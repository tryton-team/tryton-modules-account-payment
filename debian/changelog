tryton-modules-account-payment (7.0.3-4) unstable; urgency=medium

  * Add new missing test depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 31 Oct 2024 15:22:43 +0100

tryton-modules-account-payment (7.0.3-3) unstable; urgency=medium

  * Use unittest.discover to run autopkgtests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 25 Oct 2024 12:55:24 +0200

tryton-modules-account-payment (7.0.3-2) experimental; urgency=medium

  * Upload to experimental.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 21 Oct 2024 07:55:19 +0200

tryton-modules-account-payment (7.0.3-1) unstable; urgency=medium

  * Switch to pgpmode=none in the watch file.
  * Bump the Standards-Version to 4.7.0, no changes needed.
  * Setting the branch in the watch file to the fixed version 7.0.
  * Remove deprecated python3-pkg-resources from (Build)Depends (and wrap-
    and-sort -a) (Closes: #1083832).
  * Merging upstream version 7.0.3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Oct 2024 20:21:34 +0200

tryton-modules-account-payment (6.0.4-1) unstable; urgency=medium

  * Switch to pgpmode=auto in the watch file.
  * Update year of debian copyright.
  * Merging upstream version 6.0.4.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 18 Apr 2024 11:22:58 +0200

tryton-modules-account-payment (6.0.3-2) sid; urgency=medium

  * Add a salsa-ci.yml
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Repository.
  * Update standards version to 4.6.1, no changes needed.
  * Unify the Tryton module layout.
  * Update the package URLS to https and the new mono repos location.
  * Bump the Standards-Version to 4.6.2, no changes needed.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 13 Feb 2023 12:27:51 +0100

tryton-modules-account-payment (6.0.3-1) unstable; urgency=medium

  * Merging upstream version 6.0.3.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 10 Mar 2022 09:56:20 +0100

tryton-modules-account-payment (6.0.1-2) unstable; urgency=medium

  * Use debhelper-compat (=13).
  * Depend on the tryton-server-api of the same major version.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 12 Nov 2021 21:52:45 +0100

tryton-modules-account-payment (6.0.1-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.6.0, no changes needed.
  * Set the watch file version to 4.
  * Setting the branch in the watch file to the fixed version 6.0.
  * Merging upstream version 6.0.1.
  * Use same debhelper compat as for server and client.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 19 Oct 2021 12:12:13 +0200

tryton-modules-account-payment (5.0.7-1) unstable; urgency=medium

  * Updating to standards version 4.5.1, no changes needed.
  * Merging upstream version 5.0.7.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 29 Jun 2021 11:19:22 +0200

tryton-modules-account-payment (5.0.5-1) unstable; urgency=medium

  * Merging upstream version 5.0.5.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Dec 2020 09:53:17 +0100

tryton-modules-account-payment (5.0.4-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.5.0, no changes needed.
  * Add Rules-Requires-Root: no to d/control.
  * Merging upstream version 5.0.4.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 04 Jul 2020 13:06:57 +0200

tryton-modules-account-payment (5.0.3-1) unstable; urgency=medium

  * Merging upstream version 5.0.3.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Apr 2020 13:23:14 +0200

tryton-modules-account-payment (5.0.2-1) unstable; urgency=medium

  * Merging upstream version 5.0.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 03 Dec 2019 10:49:45 +0100

tryton-modules-account-payment (5.0.1-2) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Bump the Standards-Version to 4.4.0, no changes needed.
  * Update year of debian copyright.
  * Setting the branch in the watch file to the fixed version 5.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 22 Jul 2019 12:16:43 +0200

tryton-modules-account-payment (5.0.1-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.3.0, no changes needed.
  * Merging upstream version 5.0.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 20 Feb 2019 12:53:04 +0100

tryton-modules-account-payment (5.0.0-3) unstable; urgency=medium

  * Add missing test Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 03 Jan 2019 21:28:14 +0100

tryton-modules-account-payment (5.0.0-2) unstable; urgency=medium

  * Cleanup white space.
  * Add a generic autopkgtest to run the module testsuite.
  * Update the rules file with hints about and where to run tests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 01 Jan 2019 20:34:56 +0100

tryton-modules-account-payment (5.0.0-1) unstable; urgency=medium

  * Merging upstream version 5.0.0.
  * Updating copyright file.
  * Updating to Standards-Version: 4.2.1, no changes needed.
  * Update signing-key.asc with the minimized actual upstream maintainer
    key.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 20 Nov 2018 17:07:50 +0100

tryton-modules-account-payment (4.6.0-2) unstable; urgency=medium

  * Update year of debian copyright.
  * Updating to standards version 4.1.3, no changes needed.
  * control: update Vcs-Browser and Vcs-Git
  * Set the Maintainer address to team+tryton-team@tracker.debian.org.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 29 Mar 2018 21:14:32 +0200

tryton-modules-account-payment (4.6.0-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.1.0, no changes needed.
  * Bump the Standards-Version to 4.1.1, no changes needed.
  * Merging upstream version 4.6.0.
  * Use https in the watch file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 07 Nov 2017 10:19:51 +0100

tryton-modules-account-payment (4.4.0-4) unstable; urgency=medium

  * Switch to Python3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 18 Aug 2017 12:35:16 +0200

tryton-modules-account-payment (4.4.0-3) unstable; urgency=medium

  * Use the preferred https URL format in the copyright file.
  * Bump the Standards-Version to 4.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Aug 2017 16:02:34 +0200

tryton-modules-account-payment (4.4.0-2) unstable; urgency=medium

  * Change the maintainer address to tryton-debian@lists.alioth.debian.org
    (Closes: #865109).

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Jul 2017 12:35:59 +0200

tryton-modules-account-payment (4.4.0-1) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Merging upstream version 4.4.0.
  * Updating debian/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 10 Jun 2017 23:29:29 +0200

tryton-modules-account-payment (4.2.0-1) unstable; urgency=medium

  * Updating to Standards-Version: 3.9.8, no changes needed.
  * Merging upstream version 4.2.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 05 Dec 2016 15:31:07 +0100

tryton-modules-account-payment (4.0.1-1) unstable; urgency=medium

  * Updating signing-key.asc with the actual upstream maintainer keys.
  * Merging upstream version 4.0.0.
  * Merging upstream version 4.0.1.
  * Updating the copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 30 May 2016 19:27:08 +0200

tryton-modules-account-payment (3.8.0-2) unstable; urgency=medium

  * Updating to standards version 3.9.7, no changes needed.
  * Updating VCS-Browser to https and canonical cgit URL.
  * Updating VCS-Git to https and canonical cgit URL.
  * Removing the braces from the dh call.
  * Removing the version constraint from python.
  * Disable tests globally for all Python versions.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 08 Mar 2016 13:08:30 +0100

tryton-modules-account-payment (3.8.0-1) unstable; urgency=medium

  * Updating year of debian copyright.
  * Adapting section naming in gbp.conf to current git-buildpackage.
  * Improving description why we can not run the module test suites.
  * Merging upstream version 3.8.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 12 Nov 2015 19:13:07 +0100

tryton-modules-account-payment (3.6.0-1) unstable; urgency=medium

  * Wrapping and sorting control files (wrap-and-sort -bts).
  * Merging upstream version 3.6.0.
  * Updating Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 26 Apr 2015 23:49:03 +0200

tryton-modules-account-payment (3.4.1-1) unstable; urgency=medium

  * Adding actual upstream signing key.
  * Improving boilerplate in d/control (Closes: #771722).
  * Merging upstream version 3.4.1.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 03 Mar 2015 11:28:09 +0100

tryton-modules-account-payment (3.4.0-1) unstable; urgency=medium

  * Adding actual upstream signing key.
  * Updating to Standards-Version: 3.9.6, no changes needed.
  * Merging upstream version 3.4.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 21 Oct 2014 20:23:48 +0200

tryton-modules-account-payment (3.2.1-1) unstable; urgency=medium

  * Updating signing key while using now plain .asc files instead of .pgp
    binaries.
  * Merging upstream version 3.2.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 26 Aug 2014 13:16:28 +0200

tryton-modules-account-payment (3.2.0-1) unstable; urgency=medium

  * Initial release (Closes: #742736).

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 23 Apr 2014 12:58:25 +0200
